import QtQuick 2.15
import org.kde.plasma.core as PlasmaCore
import org.kde.kirigami as Kirigami

Item {
    Rectangle {
        id: bluebackground
        color: "blue"
        anchors.fill: parent
    }
}
