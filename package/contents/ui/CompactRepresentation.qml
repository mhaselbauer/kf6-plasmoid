import QtQuick 2.15
import org.kde.plasma.core as PlasmaCore
import org.kde.kirigami as Kirigami

Item {
    id: compactRep

    Rectangle {
        id: redbackground
        color: "red"
        anchors.fill: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: {
            root.expanded = !root.expanded;
        }
        hoverEnabled: true
    }
}
